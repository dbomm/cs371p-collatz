// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

//global cache used on this file
static int cache[1000000] = { 0 };

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int cycle_length (long int n) {
    int c = 1;
    while (n > 1) {
        //make sure number isn't out of range of cache array
        if (n < 1000000) {
            //utilize cache
            if (cache[n] != 0) {
                c += cache[n] - 1;
                return c;
            }
        }
        if ((n % 2) == 0) {
            n = (n >> 1);
            ++c;
        }
        else {
            n += (n >> 1) + 1;
            c += 2;
        }
    }
    return c;
}


int collatz_eval(int i, int j) {
    //ensure start <= stop
    int start, stop;
    if (j < i) {
        start = j;
        stop = i;
    }
    else {
        start = i;
        stop = j;
    }
    //math shortcut from class
    int m = (stop >> 2) + 1;
    if (start < m) {
        start = m;
    }

    int maxLength = 1;
    for (long int num = start; num <= stop; num++) {
        int length;
        if (cache[num] != 0) {
            length = cache[num];
        }
        else {
            length = cycle_length(num);
            cache[num] = length;
        }

        if (length >= maxLength) {
            maxLength = length;
        }

    }
    return maxLength;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
